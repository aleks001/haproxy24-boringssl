#!/bin/bash

set -e

if [[ -z "${SERVICE_DEST}" ]];
  then
    echo "Error the SERVICE_DEST MUST be defined"
    exit 1
fi

if [[ -z "${SERVICE_DEST_PORT}" ]];
then
  echo "Error the SERVICE_DEST_PORT MUST be defined"
  exit 1
fi

if [[ -z "${FE_TCP_PORT}" ]];
then
  echo "Error the FE_TCP_PORT MUST be defined"
  exit 1
fi
export TZ=${TZ:-UTC}
export LOGLEVEL=${LOGLEVEL:-notice}
export NUM_THREADS=${NUM_THREADS:-16}
export STATS_PORT=${STATS_PORT:-1946}
export STATS_USER=${STATS_USER:-admin}
export STATS_PASSWORD=${STATS_PASSWORD:-admin}
export SERVICE_NAME=${SERVICE_NAME:-tcp-server}
export CONFIG_FILE=${SERVICE_NAME:-/usr/local/etc/haproxy/haproxy.conf}

if [[ -n "${DEBUG}" ]]; then

 set -x

echo "Current ENV Values"
echo "==================="
echo "FE_TCP_PORT         :"${FE_TCP_PORT}
echo "SERVICE_NAME        :"${SERVICE_NAME}
echo "SERVICE_DEST        :"${SERVICE_DEST}
echo "SERVICE_DEST_PORT   :"${SERVICE_DEST_PORT}
echo "TZ                  :"${TZ}
echo "LOGLEVEL            :"${LOGLEVEL}
echo "CONFIG_FILE         :"${CONFIG_FILE}
echo "NUM_THREADS         :"${NUM_THREADS}
echo "STATS_PORT          :"${STATS_PORT}
echo "STATS_USER          :"${STATS_USER}
echo "STATS_PASSWORD      :"${STATS_PASSWORD}

echo "HAProxy Version:"

/usr/local/sbin/haproxy -vv

echo "params" $@
echo "----  -----"
echo "param1" $1
fi

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- haproxy "$@"
fi

if [ "$1" = 'haproxy' ]; then
	shift # "haproxy"
	# if the user wants "haproxy", let's add a couple useful flags
	#   -W  -- "master-worker mode" (similar to the old "haproxy-systemd-wrapper"; allows for reload via "SIGUSR2")
	#   -db -- disables background mode
	set -- haproxy -W -S /var/run/hap-master-socket -db "$@"
fi

exec "$@"