# haproxy-19-boringssl

This project builds haproxy with latest boring ssl on centos:latest

# run with this command.

```
podman run --name haproxy --rm \
  -e SERVICE_DEST=www.google.com \
  -e SERVICE_DEST_PORT=443 \
  -e FE_TCP_PORT=4711 \
  hap-24-bor -d -f /usr/local/etc/haproxy/haproxy.conf
```